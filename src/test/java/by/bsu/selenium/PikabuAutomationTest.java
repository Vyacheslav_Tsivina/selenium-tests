package by.bsu.selenium;

import by.bsu.selenium.steps.Steps;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PikabuAutomationTest {
    private Steps steps;
	private static final String SEARCH_TERM_POPULAR = "кот";
	private static final String SEARCH_TERM_UNUSUAL = "fej67Qwwq44праКdЙЙsss";
	private static final String LOGIN = "testSelenium";
	private static final String PASSWORD = "1qaz@WS";

	@BeforeMethod(description = "Init browser")
	public void setUp()
	{
		steps = new Steps();
		steps.initBrowser();
	}

	@Test(description = "Search for popular query is not empty")
	public void searchPopular()
	{
		steps.search(SEARCH_TERM_POPULAR);
		Assert.assertTrue(steps.searchIsNotEmpty());
	}

	@Test(description = "Search for unusual query is  empty")
	public void searchUnusual()
	{
		steps.search(SEARCH_TERM_UNUSUAL);
		Assert.assertFalse(steps.searchIsNotEmpty());
	}

	@Test(description = "If all posts on author posts page have tag [моё]")
	public void authorPostsPage()
	{
		steps.authorPosts();
		Assert.assertTrue(steps.checkAuthorPosts());
	}

	@Test(description = "Successful log in")
	public void successfulLogIn()
	{
		steps.login(LOGIN, PASSWORD);
		Assert.assertTrue(steps.loggedIn(LOGIN));
	}

	@Test(description = "Popular tag page")
	public void popularTagPage()
	{
		String tagName = steps.popularTag();
		Assert.assertTrue(steps.allPostsHavePopularTag(tagName));
	}

	@AfterMethod(description = "Stop Browser")
	public void stopBrowser()
	{
		steps.closeDriver();
	}
}
