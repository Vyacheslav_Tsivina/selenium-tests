package by.bsu.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage extends AbstractPage
{
	private final Logger logger = Logger.getLogger(SearchPage.class);
	private final String BASE_URL = "http://pikabu.ru/search.php";

	@FindBy(id = "ss_text")
	private WebElement searchInput;

	@FindBy(className = "story-search-submit")
	private WebElement buttonSubmit;

	@FindBy(className = "search-result-stories")
	private WebElement searchResults;

	public SearchPage(WebDriver driver)
	{
		super(driver);
		PageFactory.initElements(this.driver, this);
	}

	@Override
	public void openPage()
	{
		driver.navigate().to(BASE_URL);
		logger.info("Search page opened");
	}

	public void search(String searchTerm)
	{
		searchInput.sendKeys(searchTerm);
		buttonSubmit.click();
		logger.info("Search performed");
	}

	public String searchResults()
	{
		WebDriverWait wait = new WebDriverWait(driver,30);// wait for AJAX call
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("search-result-stories")));
		return searchResults.getText();
	}

}
