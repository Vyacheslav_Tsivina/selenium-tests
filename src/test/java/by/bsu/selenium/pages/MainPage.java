package by.bsu.selenium.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class MainPage extends AbstractPage {
    private final Logger logger = Logger.getLogger(MainPage.class);

    private final String BASE_URL = "http://pikabu.ru/";

    @FindBy(linkText = "[моё]")
    private WebElement authorPostsLink;

    @FindBy(className = "b-story__main-header")
    private List<WebElement> postsHeaders;

    @FindBy(id = "username")
    private WebElement loginInput;

    @FindBy(id = "password")
    private WebElement passwordInput;

    @FindBy(xpath = "//div[contains(@class, 'b-sign__in')]/form/div/button")
    private WebElement loginSubmit;

    @FindBy(xpath = "//div[@class='b-user-menu__header']/a")
    private WebElement userNameLink;

    @FindBy(xpath = "//td[@class='menu-top-tags-cont']/div[1]/a")
    private WebElement mostPopularTagLink;

    @FindBy(xpath = "//td[@class='menu-top-tags-cont']/div[1]/a/span[@class='tag no_ch']")
    private WebElement mostPopularTagSpan;



    public MainPage(WebDriver driver)
    {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @Override
    public void openPage() {
        driver.navigate().to(BASE_URL);
        logger.info("Main page opened");
    }

    public void openAuthorPostsPage(){
        authorPostsLink.click();
        logger.info("Author posts page opened");
    }

    public boolean allPostsAreAuthorPosts(){
        for(WebElement postHeader : postsHeaders){
            if (postHeader.findElement(By.className("story_authors")) == null){
                return  false;
            }
        }
        return true;
    }

    public void login(String login, String password){
        loginInput.sendKeys(login);
        passwordInput.sendKeys(password);
        loginSubmit.click();
        logger.info("Logged in");
    }

    public boolean loggedIn(String login){
        return login.equals(userNameLink.getText());
    }

    /**
     * Returns most popular tag name
     */
    public String popularTagPage(){
        String tagName = mostPopularTagSpan.getText();
        mostPopularTagLink.click();
        logger.info("Most popular tag page opened");
        return tagName;
    }

    public boolean allStoriesContainsPopularTag(String tagName){
        for (WebElement postHeader : postsHeaders){
            boolean hasTagName = false;
            for (WebElement tag: postHeader.findElements(By.cssSelector(".tag.no_ch"))){
                if (tagName.equalsIgnoreCase(tag.getText())){
                    hasTagName = true;
                    break;
                }
            }
            if (!hasTagName){
                return false;
            }
        }
        return true;
    }

}
