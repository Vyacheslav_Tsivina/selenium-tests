package by.bsu.selenium.steps;

import by.bsu.selenium.pages.MainPage;
import by.bsu.selenium.pages.SearchPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class Steps
{
	private WebDriver driver;

	private final Logger logger = Logger.getLogger(Steps.class);

	public void initBrowser()
	{
		driver = new FirefoxDriver();
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		logger.info("Browser started");
	}

	public void closeDriver()
	{
		driver.quit();
	}

	public void search(String searchTerm)
	{
		SearchPage searchPage = new SearchPage(driver);
		searchPage.openPage();
		searchPage.search(searchTerm);
	}

	public boolean searchIsNotEmpty()
	{
		SearchPage searchPage = new SearchPage(driver);
		return !searchPage.searchResults().isEmpty();
	}

	public void authorPosts(){
		MainPage mainPage = new MainPage(driver);
		mainPage.openPage();
		mainPage.openAuthorPostsPage();
	}

	public boolean checkAuthorPosts(){
		MainPage mainPage = new MainPage(driver);
		return mainPage.allPostsAreAuthorPosts();
	}

	public void login(String login, String password){
		MainPage mainPage = new MainPage(driver);
		mainPage.openPage();
		mainPage.login(login, password);
	}

	public boolean loggedIn(String login){
		MainPage mainPage = new MainPage(driver);
		return mainPage.loggedIn(login);
	}

	public String popularTag(){
		MainPage mainPage = new MainPage(driver);
		mainPage.openPage();
		return mainPage.popularTagPage();
	}

	public boolean allPostsHavePopularTag(String tagName){
		MainPage mainPage = new MainPage(driver);
		return mainPage.allStoriesContainsPopularTag(tagName);
	}
}
